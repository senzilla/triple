# SPDX-License-Identifier: ISC

BIN_TARGETS=		triple		\
			triple-fmt	\
			triple-text	\
			triple-turtle	\
			triple-rdfa
