triple - rdfa parser written in awk
===================================

An RDFa parser written in awk, capable of outputting the results in
different formats such as Turtle - Terse RDF Triple Language.
